/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fudgydrs.scrapesc6htm;

/**
 *
 * @author yetim
 */
public class Attack_command {
    private String attack_name;
    private String attack_command;
    private String jp_name;
    private String property;
    private String damage;
    private String startup;
    private String block;
    private String hit;
    private String counter_hit;
    private String chip_damage;
    private String guard_break;
    private String notes;
    private String index;

    public Attack_command() {
        this.attack_name = "";
        this.attack_command = "";
        this.jp_name = "";
        this.property = "";
        this.damage = "";
        this.startup = "";
        this.block = "";
        this.hit = "";
        this.counter_hit = "";
        this.chip_damage = "";
        this.guard_break = "";
        this.notes = "";
        this.index = "";
    }
    
    public Attack_command(String attack_name, String attack_command, String jp_name, String property, String damage, String startup, String block, String hit, String counter_hit, String chip_damage, String guard_break, String notes, String index) {
        this.attack_name = attack_name;
        this.attack_command = attack_command;
        this.jp_name = jp_name;
        this.property = property;
        this.damage = damage;
        this.startup = startup;
        this.block = block;
        this.hit = hit;
        this.counter_hit = counter_hit;
        this.chip_damage = chip_damage;
        this.guard_break = guard_break;
        this.notes = notes;
        this.index = index;
    }
    
    @Override
    public String toString() {
        String str = "___________________________\n"
                + "Attack Name: " + this.attack_name + "\n"
                + "Attack Command: " + this.attack_command + "\n"
                + "JP Name: " + this.jp_name + "\n"
                + "Property: " + this.property + "\n"
                + "Damage: " + this.damage + "\n"
                + "Start-Up: " + this.startup + "\n"
                + "Block: " + this.block + "\n"
                + "NH: " + this.hit + "\n"
                + "CH: " + this.counter_hit + "\n"
                + "Chip: " + this.chip_damage + "\n"
                + "GB: " + this.guard_break + "\n"
                + "Notes: " + this.notes + "\n"
                + "Index: " + this.index;
        return str;
    }
            
    public void setAttack_command(String attack_command) {
        this.attack_command = attack_command;
    }

    public String getAttack_command() {
        return attack_command;
    }
    
    public void setAttack_name(String attack_name) {
        this.attack_name = attack_name;
    }

    public void setJp_name(String jp_name) {
        this.jp_name = jp_name;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public void setDamage(String damage) {
        this.damage = damage;
    }

    public void setStartup(String startup) {
        this.startup = startup;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public void setHit(String hit) {
        this.hit = hit;
    }

    public void setCounter_hit(String counter_hit) {
        this.counter_hit = counter_hit;
    }

    public void setChip_damage(String chip_damage) {
        this.chip_damage = chip_damage;
    }

    public void setGuard_break(String guard_break) {
        this.guard_break = guard_break;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getAttack_name() {
        return attack_name;
    }

    public String getJp_name() {
        return jp_name;
    }

    public String getProperty() {
        return property;
    }

    public String getDamage() {
        return damage;
    }

    public String getStartup() {
        return startup;
    }

    public String getBlock() {
        return block;
    }

    public String getHit() {
        return hit;
    }

    public String getCounter_hit() {
        return counter_hit;
    }

    public String getChip_damage() {
        return chip_damage;
    }

    public String getGuard_break() {
        return guard_break;
    }

    public String getNotes() {
        return notes;
    }

    public String getIndex() {
        return index;
    }
    
    
}




