
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fudgydrs.scrapesc6htm;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.util.ArrayList;


/**
 *
 * @author yetim
 */
public class main {

    /*
    private String attack_name;
    private String jp_name;
    private String property;
    private String damage;
    private String startup;
    private String block;
    private String hit;
    private String counter_hit;
    private String chip_damage;
    private String guard_break;
    private String notes;
    private String index;
    a b k c fc j ra rb rk rc bt stances t re
     */
    //("<a name=\"a\"></a>\n    <table class=\"fr\">", "<table id=\"a\">")
    //("<a name=\"b\"></a>\n    <table class=\"fr\">", "<table id=\"b\">")
    //("<a name=\"k\"></a>\n    <table class=\"fr\">", "<table id=\"k\">")
    //("<a name=\"c\"></a>\n    <table class=\"fr\">", "<table id=\"c\">")
    //("<a name=\"fc\"></a>\n    <table class=\"fr\">", "<table id=\"fc\">")
    //("<a name=\"j\"></a>\n    <table class=\"fr\">", "<table id=\"j\">")
    //("<a name=\"ra\"></a>\n    <table class=\"fr\">", "<table id=\"ra\">")
    //("<a name=\"rb\"></a>\n    <table class=\"fr\">", "<table id=\"rb\">")
    //("<a name=\"rk\"></a>\n    <table class=\"fr\">", "<table id=\"rk\">")
    //("<a name=\"rc\"></a>\n    <table class=\"fr\">", "<table id=\"rc\">")
    //("<a name=\"bt\"></a>\n    <table class=\"fr\">", "<table id=\"bt\">")
    //("<a name=\"???\"></a>\n    <table class=\"fr\">", "<table id=\"???\">")
    //("<a name=\"t\"></a>\n    <table class=\"fr\">", "<table id=\"t\">")
    //("<a name=\"re\"></a>\n    <table class=\"fr\">", "<table id=\"re\">")
    //Html.fromHtml("../../../../assets/soulcalibur6/table.htm").toString();
    /*
<tr><td class="labeled"><span class="sp ce">CE</span><br />
デュミンガック・ナガ・ル・ウノス<br />
<span class="command">A</span>+<span class="command">B</span>+<span class="command">K</span><br />
<span class="level md">中</span><span class="level md">中</span><span class="level md">中</span><span class="level ta">打投</span>
 / 20,10,20(,50)</td>
<td align="center">20F</td>
<td align="center">+4</td>
<td align="center">打投</td>
<td align="center">打投</td>
<td align="center">95</td>
<td><span class="sp js">JS</span>4F<span class="sp gc">GC</span>23%<span class="sp ml">技L</span>強,強,強<br />
1ゲージを消費してクリティカルエッジを発動<br />
発生19Fから発生保証<br />
1～3段までのどれでもガードクラッシュ可<br />
ダメージは1段目から：70、2段目から：80、3段目から：80<br />
ヒットでタリム側の体力回復<br />
2段目ガードから発生2F以内で割り込み可（3F目に判定）<br />
<div class="uaccbox"><input type="checkbox" id="label78" class="cssuacc" /><label for="label78">旧アップデート</label><div class="uaccshow">(Ver.1.11)1段目のガードクラッシュ値減少、2段目のガードクラッシュ値増加。<br />
</div></div><span class="patch">(Ver.1.40)近距離の空振り緩和でヒットサイズや追尾性能を調整。<br />
(Ver.1.50)演出後の攻撃発生タイミングが他キャラより1F遅く、発動演出を視認してGIできたのを修正。<br />
無敵状態になる時間を1F遅くした。</span></td></tr>
     
    .replaceAll("<br />\n<span", "</td>\n<td><span")
    .replaceAll("<span class="level high">上</span>", "H")
    .replaceAll("<span class="level md">中</span>", "M")
    .replaceAll("<span class="level low">下</span>", "L")
    .replaceAll("<span class="level ss">特殊</span>", "(Special)")
    .replaceAll("<span class="level ss">構え</span>", "(Special)")
    .replaceAll("<span class="level sm">特中</span>", "Sm")
    .replaceAll("<span class="level sl">特下</span>", "Sl")
    .replaceAll("<span class="level re">RE</span>", "(RE)")
    .replaceAll("<span class="level mid_ua">中段ガード不能</span>", "M!")
    .replaceAll("<span class="level ta">打投</span>", "(TA)")
    .replaceAll("<span class="level th">上投</span>", "(Throw)")
    
    .replaceAll("&thinsp;H", " H")
    .replaceAll("&thinsp;M", " M")
    .replaceAll("&thinsp;L", " L")
    .replaceAll("&thinsp;Sm", " Sm")
    .replaceAll("&thinsp;Sl", " Sl")
    .replaceAll("&thinsp;H!", " H!")
    .replaceAll("&thinsp;M!", " M!")
    .replaceAll("&thinsp;L!", " L!")
    .replaceAll("&thinsp;Sm!", " Sm!")
    .replaceAll("&thinsp;Sl!", " Sl!")
    .replaceAll("&thinsp;(RE)", " (RE)")
    .replaceAll("&thinsp;(Special)", " (Special)")
    .replaceAll("&thinsp;(TA)", " (TA)")
    .replaceAll("&thinsp;(Throw)", " (Throw)")
    
    .replaceAll("<span class="command hold">A</span>+<span class="command hold">B</span>ホールド", "[A+B]")
    .replaceAll("<span class="command hold">A</span>+<span class="command hold">K</span>ホールド", "[A+K]")
    .replaceAll("<span class="command hold">A</span>+<span class="command hold">G</span>ホールド", "[A+G]")
    .replaceAll("<span class="command hold">A</span>+<span class="command hold">B</span>+<span class="command hold">K</span>ホールド", "[A+B+K]")
    .replaceAll("<span class="command hold">A</span>+<span class="command hold">B</span>+<span class="command hold">G</span>ホールド", "[A+B+G]")
    .replaceAll("<span class="command hold">A</span>+<span class="command hold">B</span>+<span class="command hold">K</span>+<span class="command hold">G</span>ホールド", "[A+B+K+G]")
    .replaceAll("<span class="command hold">A</span>+<span class="command hold">K</span>+<span class="command hold">G</span>ホールド", "[A+K+G]")
    .replaceAll("<span class="command hold">B</span>+<span class="command hold">K</span>ホールド", "[B+K]")
    .replaceAll("<span class="command hold">B</span>+<span class="command hold">G</span>ホールド", "[B+G]")
    .replaceAll("<span class="command hold">B</span>+<span class="command hold">K</span>+<span class="command hold">G</span>ホールド", "[B+K+G]")
    .replaceAll("<span class="command hold">K</span>+<span class="command hold">G</span>ホールド", "[K+G]")
   
    .replaceAll("<span class="command">A</span>+<span class="command">B</span>", "A+B")
    .replaceAll("<span class="command">A</span>+<span class="command">K</span>", "A+K")
    .replaceAll("<span class="command">A</span>+<span class="command">G</span>", "A+G")
    .replaceAll("<span class="command">A</span>+<span class="command">B</span>+<span class="command">K</span>", "A+B+K")
    .replaceAll("<span class="command">A</span>+<span class="command">B</span>+<span class="command">G</span>", "A+B+G")
    .replaceAll("<span class="command">A</span>+<span class="command">B</span>+<span class="command">K</span>+<span class="command">G</span>", "A+B+K+G")
    .replaceAll("<span class="command">A</span>+<span class="command">K</span>+<span class="command">G</span>", "A+K+G")
    .replaceAll("<span class="command">B</span>+<span class="command">K</span>", "B+K")
    .replaceAll("<span class="command">B</span>+<span class="command">G</span>", "B+G")
    .replaceAll("<span class="command">B</span>+<span class="command">K</span>+<span class="command">G</span>", "B+K+G")
    .replaceAll("<span class="command">K</span>+<span class="command">G</span>", "K+G")
   
    .replaceAll("<span class="command">A</span>", "A")
    .replaceAll("<span class="command hold">A</span>ホールド", "[A]")
    .replaceAll("<span class="command">B</span>", "B")
    .replaceAll("<span class="command hold">B</span>ホールド", "[B]")
    .replaceAll("<span class="command">K</span>", "K")
    .replaceAll("<span class="command hold">K</span>ホールド", "[K]")
    .replaceAll("<span class="command">G</span>", "G")
    .replaceAll("<span class="command hold">G</span>ホールド", "[G]")
   
    .replaceAll("&thinsp;or&thinsp;", "")
   
    .replaceAll("<img src="../1'.bmp" class="c"/>", "[1]")
    .replaceAll("<img src="../2'.bmp" class="c"/>", "[2]")
    .replaceAll("<img src="../3'.bmp" class="c"/>", "[3]")
    .replaceAll("<img src="../4'.bmp" class="c"/>", "[4]")
    .replaceAll("<img src="../6'.bmp" class="c"/>", "[6]")
    .replaceAll("<img src="../7'.bmp" class="c"/>", "[7]")
    .replaceAll("<img src="../8'.bmp" class="c"/>", "[8]")
    .replaceAll("<img src="../9'.bmp" class="c"/>", "[9]")
   
    .replaceAll("<img src="../1.bmp" class="c" />", "1")
    .replaceAll("<img src="../2.bmp" class="c" />", "2")
    .replaceAll("<img src="../3.bmp" class="c" />", "3")
    .replaceAll("<img src="../4.bmp" class="c" />", "4")
    .replaceAll("<img src="../6.bmp" class="c" />", "6")
    .replaceAll("<img src="../7.bmp" class="c" />", "7")
    .replaceAll("<img src="../8.bmp" class="c" />", "8")
    .replaceAll("<img src="../9.bmp" class="c" />", "9")
   
    .replaceAll(" / -", "</td><td>")
    .replaceAll(" / 0", "</td><td>")
    .replaceAll(" / 1", "</td><td>")
    .replaceAll(" / 2", "</td><td>")
    .replaceAll(" / 3", "</td><td>")
    .replaceAll(" / 4", "</td><td>")
    .replaceAll(" / 5", "</td><td>")
    .replaceAll(" / 6", "</td><td>")
    .replaceAll(" / 7", "</td><td>")
    .replaceAll(" / 8", "</td><td>")
    .replaceAll(" / 9", "</td><td>")
    
    .replaceAll("<sup>S</sup>", "s")
    .replaceAll("<sup>Y</sup>", "y")
    .replaceAll("<sup>B</sup>", "b")
    .replaceAll("<sup>G</sup>", "g")
    .replaceAll("<sup>K</sup>", "k")
    
    .replaceAll("<td align="center">浮</td>", "<td align="center">LNC</td>")
    
    .replaceAll("<span class="command_s bs2 hold">B+G</span>", "B+G:")
    .replaceAll("<span class="command_s bs2 hold">A+B</span>", "A+B:")
    .replaceAll("<span class="command_s bs1 hold">A</span>", "A:")
    .replaceAll("<span class="command_s bs1 hold">B</span>", "B:")
    .replaceAll("<span class="command_s bs1 hold">K</span>", "K:")
    .replaceAll("<span class="command_s bs1 hold">G</span>", "G:")
    
    .replaceAll("H H", "H, H")
    .replaceAll("H M", "H, M")
    .replaceAll("H L", "H, L")
    .replaceAll("H Sm", "H, Sm")
    .replaceAll("H Sl", "H, Sl")
    .replaceAll("M H", "M, H")
    .replaceAll("M M", "M, M")
    .replaceAll("M L", "M, L")
    .replaceAll("M Sm", "M, Sm")
    .replaceAll("M Sl", "M, Sl")
    .replaceAll("L H", "L, H")
    .replaceAll("L M", "L, M")
    .replaceAll("L L", "L, L")
    .replaceAll("L Sm", "L, Sm")
    .replaceAll("L Sl", "L, Sl")
    .replaceAll("Sm H", "Sm, H")
    .replaceAll("Sm M", "Sm, M")
    .replaceAll("Sm L", "Sm, L")
    .replaceAll("Sm Sm", "Sm, Sm")
    .replaceAll("Sm Sl", "Sm, Sl")
    .replaceAll("Sl H", "Sl, H")
    .replaceAll("Sl M", "Sl, M")
    .replaceAll("Sl L", "Sl, L")
    .replaceAll("Sl Sm", "Sl, Sm")
    .replaceAll("Sl Sl", "Sl, Sl")
    .replaceAll("! H", "!, H")
    .replaceAll("! M", "!, M")
    .replaceAll("! L", "!, L")
    .replaceAll("! Sm", "!, Sm")
    .replaceAll("! Sl", "!, Sl")
    
    .replaceAll("AA", "A.A")
    .replaceAll("AA", "A.A")
    .replaceAll("AA", "A.A")
    .replaceAll("AA", "A.A")
    .replaceAll("AA", "A.A")
    .replaceAll("AA", "A.A")
    .replaceAll("AB", "A.B")
    .replaceAll("AK", "A.K")
    .replaceAll("AG", "A.G")
    .replaceAll("BA", "B.A")
    .replaceAll("BB", "B.B")
    .replaceAll("BB", "B.B")
    .replaceAll("BB", "B.B")
    .replaceAll("BB", "B.B")
    .replaceAll("BB", "B.B")
    .replaceAll("BB", "B.B")
    .replaceAll("BK", "B.K")
    .replaceAll("BG", "B.G")
    .replaceAll("KA", "K.A")
    .replaceAll("KB", "K.B")
    .replaceAll("KK", "K.K")
    .replaceAll("KK", "K.K")
    .replaceAll("KK", "K.K")
    .replaceAll("KK", "K.K")
    .replaceAll("KK", "K.K")
    .replaceAll("KK", "K.K")
    .replaceAll("KK", "K.K")
    .replaceAll("KG", "K.G")
    .replaceAll("GA", "G.A")
    .replaceAll("GB", "G.B")
    .replaceAll("GK", "G.K")
    .replaceAll("GG", "G.G")
    .replaceAll("A\\[", "A.[")
    .replaceAll("B\\[", "B.[")
    .replaceAll("K\\[", "K.[")
    .replaceAll("G\\[", "G.[")
    
    .replaceAll("A1", "A.1")
    .replaceAll("A2", "A.2")
    .replaceAll("A3", "A.3")
    .replaceAll("A4", "A.4")
    .replaceAll("A5", "A.5")
    .replaceAll("A6", "A.6")
    .replaceAll("A7", "A.7")
    .replaceAll("A8", "A.8")
    .replaceAll("A9", "A.9")
    .replaceAll("B1", "B.1")
    .replaceAll("B2", "B.2")
    .replaceAll("B3", "B.3")
    .replaceAll("B4", "B.4")
    .replaceAll("B5", "B.5")
    .replaceAll("B6", "B.6")
    .replaceAll("B7", "B.7")
    .replaceAll("B8", "B.8")
    .replaceAll("B9", "B.9")
    .replaceAll("K1", "K.1")
    .replaceAll("K2", "K.2")
    .replaceAll("K3", "K.3")
    .replaceAll("K4", "K.4")
    .replaceAll("K5", "K.5")
    .replaceAll("K6", "K.6")
    .replaceAll("K7", "K.7")
    .replaceAll("K8", "K.8")
    .replaceAll("K9", "K.9")
    .replaceAll("G1", "G.1")
    .replaceAll("G2", "G.2")
    .replaceAll("G3", "G.3")
    .replaceAll("G4", "G.4")
    .replaceAll("G5", "G.5")
    .replaceAll("G6", "G.6")
    .replaceAll("G7", "G.7")
    .replaceAll("G8", "G.8")
    .replaceAll("G9", "G.9")
    
    .replaceAll("&plusmn;0", "0")
    .replaceAll("SC中", "SC ")
    
    .replaceAll("背向けA", "BT A")
    .replaceAll("背向けB", "BT B")
    .replaceAll("背向けK", "BT K")
    .replaceAll("背向けG", "BT G")
    .replaceAll("背向け\\[", "BT [")
    .replaceAll("背向け1", "BT 1")
    .replaceAll("背向け2", "BT 2")
    .replaceAll("背向け3", "BT 3")
    .replaceAll("背向け4", "BT 4")
    .replaceAll("背向け5", "BT 5")
    .replaceAll("背向け6", "BT 6")
    .replaceAll("背向け7", "BT 7")
    .replaceAll("背向け8", "BT 8")
    .replaceAll("背向け9", "BT 9")
    
    .replaceAll("しゃがみA", "FC A")
    .replaceAll("しゃがみB", "FC B")
    .replaceAll("しゃがみK", "FC K")
    .replaceAll("しゃがみG", "FC G")
    .replaceAll("しゃがみ\\[", "FC [")
    .replaceAll("しゃがみ1", "FC 1")
    .replaceAll("しゃがみ2", "FC 2")
    .replaceAll("しゃがみ3", "FC 3")
    .replaceAll("しゃがみ4", "FC 4")
    .replaceAll("しゃがみ5", "FC 5")
    .replaceAll("しゃがみ6", "FC 6")
    .replaceAll("しゃがみ7", "FC 7")
    .replaceAll("しゃがみ8", "FC 8")
    .replaceAll("しゃがみ9", "FC 9")
    
    .replaceAll("立ち途中A", "WR A")
    .replaceAll("立ち途中B", "WR B")
    .replaceAll("立ち途中K", "WR K")
    .replaceAll("立ち途中G", "WR G")
    .replaceAll("立ち途中\\[", "WR [")
    .replaceAll("立ち途中1", "WR 1")
    .replaceAll("立ち途中2", "WR 2")
    .replaceAll("立ち途中3", "WR 3")
    .replaceAll("立ち途中4", "WR 4")
    .replaceAll("立ち途中5", "WR 5")
    .replaceAll("立ち途中6", "WR 6")
    .replaceAll("立ち途中7", "WR 7")
    .replaceAll("立ち途中8", "WR 8")
    .replaceAll("立ち途中9", "WR 9")
    
    .replaceAll("ジャンプ中A", "JUMP A")
    .replaceAll("ジャンプ中B", "JUMP B")
    .replaceAll("ジャンプ中K", "JUMP K")
    .replaceAll("ジャンプ中G", "JUMP G")
    .replaceAll("ジャンプ中\\[", "JUMP [")
    .replaceAll("ジャンプ中1", "JUMP 1")
    .replaceAll("ジャンプ中2", "JUMP 2")
    .replaceAll("ジャンプ中3", "JUMP 3")
    .replaceAll("ジャンプ中4", "JUMP 4")
    .replaceAll("ジャンプ中5", "JUMP 5")
    .replaceAll("ジャンプ中6", "JUMP 6")
    .replaceAll("ジャンプ中7", "JUMP 7")
    .replaceAll("ジャンプ中8", "JUMP 8")
    .replaceAll("ジャンプ中9", "JUMP 9")
    
    .replaceAll("ディレイA", ", Delay A")
    .replaceAll("ディレイB", ", Delay B")
    .replaceAll("ディレイK", ", Delay K")
    .replaceAll("ディレイG", ", Delay G")
    .replaceAll("ディレイ\\[", ", Delay \\[")
    .replaceAll("ディレイ1", ", Delay 1")
    .replaceAll("ディレイ2", ", Delay 2")
    .replaceAll("ディレイ3", ", Delay 3")
    .replaceAll("ディレイ4", ", Delay 4")
    .replaceAll("ディレイ5", ", Delay 5")
    .replaceAll("ディレイ6", ", Delay 6")
    .replaceAll("ディレイ7", ", Delay 7")
    .replaceAll("ディレイ8", ", Delay 8")
    .replaceAll("ディレイ9", ", Delay 9")
    .replaceAll("ジャスト", "(Just)")
    
    .replaceAll("打投", "Throw")
    .replaceAll("ガード硬直", "Block: ")
    
    .replaceAll("\\]A", "\\].A")
    .replaceAll("\\]B", "\\].B")
    .replaceAll("\\]K", "\\].K")
    .replaceAll("\\]G", "\\].G")
    .replaceAll("\\]\\[", "\\].\\[")
    .replaceAll("\\]1", "\\].1")
    .replaceAll("\\]2", "\\].2")
    .replaceAll("\\]3", "\\].3")
    .replaceAll("\\]4", "\\].4")
    .replaceAll("\\]5", "\\].5")
    .replaceAll("\\]6", "\\].6")
    .replaceAll("\\]7", "\\].7")
    .replaceAll("\\]8", "\\].8")
    .replaceAll("\\]9", "\\].9")
    
    .replace("<span class="sp js">JS</span>", ", JS: ")
    .replace("<span class="sp gc">GC</span>", ", GC: ")
    .replace("<span class="sp cs">CS</span>", ", CS: ")
    
    AT中
    AS中
    .replace("<span class="sp ml">技L</span>", " KNB: ")
    .replace("弱", "Weak")
    中 -> med knb
    強 -> strong knb
    ヒット・ガード時 -> when hit or on block
    
    .replace("ディレイ幅", "Delayable ")
    .replace("ヒット確認猶予", "Hit confirm delayable ")
    
    .replaceAll("位置入れ替え", " Switch sides ")
    .replaceAll("ガード時、", "On block,")
    .replaceAll("ヒット時、", "On hit,")
    
    .replaceAll("1段目から連続ヒット", ", NC")
    .replaceAll("<span style=\"color:dimgray;\">", "")
    .replaceAll("%</span>", "%")
    
    .replaceAll("<input type=\"checkbox\" id=\"label119\" class=\"cssuacc\"/><label for=\"label119\">旧アップデート</label>", "Update: ")
    .replaceAll("1 or 2段目カウンターから連続ヒット", ", First two hits NCC, Second two hits NCC")
    .replaceAll("2 or 3段目カウンターから連続ヒット", ", Second two hits NCC, Third two hits NCC")
    .replaceAll("1 or 3段目カウンターから連続ヒット", ", First two hits NCC, Third two hits NCC")
    .replaceAll("1段目カウンターから連続ヒット", ", First two hits NCC")
    .replaceAll("2段目カウンターから連続ヒット", ", Second two hits NCC")
    .replaceAll("3段目カウンターから連続ヒット", ", Third two hits NCC")
    .replaceAll("1 or 2段目から連続ヒット", ", First two hits NC, Second two hits NC")
    .replaceAll("2 or 3段目から連続ヒット", ", Second two hits NC, Third two hits NC")
    .replaceAll("1 or 3段目から連続ヒット", ", First two hits NC, Third two hits NC")
    .replaceAll("1段目から連続ヒット", ", First two hits NC")
    .replaceAll("2段目から連続ヒット", ", Second two hits NC")
    .replaceAll("3段目から連続ヒット", ", Third two hits NC")
    .replaceAll("カウンターで強制しゃがみ", "")
    
    .replaceAll("ダメージ()", ", Damage ()")
    .replaceAll("<input type="checkbox" id="label6" class="cssuacc" /><label for="label6">旧アップデート</label>", "")
    .replaceAll("<td>, ", "<td>")
    .replaceAll("<br/>, ", ", ")
    */
    public static void main(String[] args) throws FileNotFoundException, IOException {

        ArrayList<String> c = new ArrayList<>();
        c.add("2b"); // 0
        c.add("amy"); // 1
        c.add("astaroth"); // 2
        c.add("azwel"); // 3
        c.add("cervantes"); // 4
        c.add("geralt"); // 5
        c.add("groh"); // 6
        c.add("inferno"); // 7
        c.add("ivy"); // 8
        c.add("kilik"); // 9
        c.add("maxi"); // 10
        c.add("mitsurugi"); // 11
        c.add("nightmare"); // 12
        c.add("raphael"); // 13
        c.add("seong_mi_na"); // 14
        c.add("siegfried"); // 15
        c.add("sophitia"); // 16
        c.add("taki"); // 17
        c.add("talim"); // 18
        c.add("tira"); // 19
        c.add("voldo"); // 20
        c.add("xianghua"); // 21
        c.add("yoshimitsu"); // 22
        c.add("zasalamel"); // 23
        c.add("cassandra"); // 24
        c.add("hilde"); // 25
        String character = c.get(25);
        File input = new File("C:\\Users\\yetim\\Documents\\NetBeansProjects\\scraper\\src\\main\\assets\\soulcalibur6\\sc6_" + character + "_data.html");
        String str = "C:\\Users\\yetim\\Documents\\NetBeansProjects\\scraper\\src\\main\\assets\\soulcalibur6\\sc6_" + character + "_data_output.html";
        String str2 = "C:\\Users\\yetim\\Documents\\NetBeansProjects\\scraper\\src\\main\\assets\\soulcalibur6\\" + character + "_raw.json";
        
        Document doc = Jsoup.parse(input, "UTF-8", "http://geppopotamus.info/");
        String output;
        output = doc.html()
                .replaceAll("<span class=\"level high\">上</span>", "H")
                .replaceAll("<span class=\"level md\">中</span>", "M")
                .replaceAll("<span class=\"level low\">下</span>", "L")
                .replaceAll("<span class=\"level ss\">特殊</span>", "(Special)")
                .replaceAll("<span class=\"level ss\">構え</span>", "(Special)")
                .replaceAll("<span class=\"level sm\">特中</span>", "Sm")
                .replaceAll("<span class=\"level sl\">特下</span>", "Sl")
                .replaceAll("<span class=\"level re\">RE</span>", "(RE)")
                .replaceAll("<span class=\"level_low_ua\">下段ガード不能</span>", "L!")
                .replaceAll("<span class=\"level mid_ua\">中段ガード不能</span>", "M!")
                .replaceAll("<span class=\"level high_ua\">上段ガード不能</span>", "H!")
                .replaceAll("<span class=\"level sm_ua\">特中段ガード不能</span>", "Sm!")
                .replaceAll("<span class=\"level sl_ua\">特下段ガード不能</span>", "Sl!")
                .replaceAll("<span class=\"level ta\">打投</span>", "(TA)")
                .replaceAll("<span class=\"level th\">上投</span>", "(High throw)")
                .replaceAll("<span class=\"level ta\">空投</span>", "(Air throw)")
                .replaceAll("<span class=\"level tl\">下投</span>", "(Low throw)")
                .replaceAll("<span class=\"level th\">連投</span>", "(Chain throw)")
                .replaceAll("&thinsp;H", " H")
                .replaceAll("&thinsp;M", " M")
                .replaceAll("&thinsp;L", " L")
                .replaceAll("&thinsp;Sm", " Sm")
                .replaceAll("&thinsp;Sl", " Sl")
                .replaceAll("&thinsp;H!", " H!")
                .replaceAll("&thinsp;M!", " M!")
                .replaceAll("&thinsp;L!", " L!")
                .replaceAll("&thinsp;Sm!", " Sm!")
                .replaceAll("&thinsp;Sl!", " Sl!")
                .replaceAll("&thinsp;(RE)", " (RE)")
                .replaceAll("&thinsp;(Special)", " (Special)")
                .replaceAll("&thinsp;(TA)", " (TA)")
                .replaceAll("&thinsp;(Throw)", " (Throw)")
                .replace("<span class=\"command hold\">A</span>+<span class=\"command hold\">B</span>ホールド", "[A+B]")
                .replace("<span class=\"command hold\">A</span>+<span class=\"command hold\">K</span>ホールド", "[A+K]")
                .replace("<span class=\"command hold\">A</span>+<span class=\"command hold\">G</span>ホールド", "[A+G]")
                .replace("<span class=\"command hold\">A</span>+<span class=\"command hold\">B</span>+<span class=\"command hold\">K</span>ホールド", "[A+B+K]")
                .replace("<span class=\"command hold\">A</span>+<span class=\"command hold\">B</span>+<span class=\"command hold\">G</span>ホールド", "[A+B+G]")
                .replace("<span class=\"command hold\">A</span>+<span class=\"command hold\">B</span>+<span class=\"command hold\">K</span>+<span class=\"command hold\">G</span>ホールド", "[A+B+K+G]")
                .replace("<span class=\"command hold\">A</span>+<span class=\"command hold\">K</span>+<span class=\"command hold\">G</span>ホールド", "[A+K+G]")
                .replace("<span class=\"command hold\">B</span>+<span class=\"command hold\">K</span>ホールド", "[B+K]")
                .replace("<span class=\"command hold\">B</span>+<span class=\"command hold\">G</span>ホールド", "[B+G]")
                .replaceAll("<span class=\"command hold\">B</span>+<span class=\"command hold\">K</span>+<span class=\"command hold\">G</span>ホールド", "[B+K+G]")
                .replaceAll("<span class=\"command hold\">K</span>+<span class=\"command hold\">G</span>ホールド", "[K+G]")
                .replaceAll("<span class=\"command\">A</span>+<span class=\"command\">B</span>", "A+B")
                .replaceAll("<span class=\"command\">A</span>+<span class=\"command\">K</span>", "A+K")
                .replaceAll("<span class=\"command\">A</span>+<span class=\"command\">G</span>", "A+G")
                .replaceAll("<span class=\"command\">A</span>+<span class=\"command\">B</span>+<span class=\"command\">K</span>", "A+B+K")
                .replaceAll("<span class=\"command\">A</span>+<span class=\"command\">B</span>+<span class=\"command\">G</span>", "A+B+G")
                .replaceAll("<span class=\"command\">A</span>+<span class=\"command\">B</span>+<span class=\"command\">K</span>+<span class=\"command\">G</span>", "A+B+K+G")
                .replaceAll("<span class=\"command\">A</span>+<span class=\"command\">K</span>+<span class=\"command\">G</span>", "A+K+G")
                .replaceAll("<span class=\"command\">B</span>+<span class=\"command\">K</span>", "B+K")
                .replaceAll("<span class=\"command\">B</span>+<span class=\"command\">G</span>", "B+G")
                .replaceAll("<span class=\"command\">B</span>+<span class=\"command\">K</span>+<span class=\"command\">G</span>", "B+K+G")
                .replaceAll("<span class=\"command\">K</span>+<span class=\"command\">G</span>", "K+G")
                .replaceAll("<span class=\"command\">A</span>", "A")
                .replaceAll("<span class=\"command hold\">A</span>ホールド", "\\[A\\]")
                .replaceAll("<span class=\"command\">B</span>", "B")
                .replaceAll("<span class=\"command hold\">B</span>ホールド", "\\[B\\]")
                .replaceAll("<span class=\"command\">K</span>", "K")
                .replaceAll("<span class=\"command hold\">K</span>ホールド", "\\[K\\]")
                .replaceAll("<span class=\"command\">G</span>", "G")
                .replaceAll("<span class=\"command hold\">G</span>ホールド", "\\[G\\]")
                .replaceAll("&thinsp;or&thinsp;", "")
                .replace("<img src=\"../1'.bmp\" class=\"c\" />", "[1]")
                .replace("<img src=\"../2'.bmp\" class=\"c\" />", "[2]")
                .replace("<img src=\"../3'.bmp\" class=\"c\" />", "[3]")
                .replace("<img src=\"../4'.bmp\" class=\"c\" />", "[4]")
                .replace("<img src=\"../6'.bmp\" class=\"c\" />", "[6]")
                .replace("<img src=\"../7'.bmp\" class=\"c\" />", "[7]")
                .replace("<img src=\"../8'.bmp\" class=\"c\" />", "[8]")
                .replace("<img src=\"../9'.bmp\" class=\"c\" />", "[9]")
                .replaceAll("<img src=\"../1.bmp\" class=\"c\" />", "1")
                .replaceAll("<img src=\"../2.bmp\" class=\"c\" />", "2")
                .replaceAll("<img src=\"../3.bmp\" class=\"c\" />", "3")
                .replaceAll("<img src=\"../4.bmp\" class=\"c\" />", "4")
                .replaceAll("<img src=\"../6.bmp\" class=\"c\" />", "6")
                .replaceAll("<img src=\"../7.bmp\" class=\"c\" />", "7")
                .replaceAll("<img src=\"../8.bmp\" class=\"c\" />", "8")
                .replaceAll("<img src=\"../9.bmp\" class=\"c\" />", "9")
                .replace("<img src=\"../1'.bmp\" class=\"c\">", "[1]")
                .replace("<img src=\"../2'.bmp\" class=\"c\">", "[2]")
                .replace("<img src=\"../3'.bmp\" class=\"c\">", "[3]")
                .replace("<img src=\"../4'.bmp\" class=\"c\">", "[4]")
                .replace("<img src=\"../6'.bmp\" class=\"c\">", "[6]")
                .replace("<img src=\"../7'.bmp\" class=\"c\">", "[7]")
                .replace("<img src=\"../8'.bmp\" class=\"c\">", "[8]")
                .replace("<img src=\"../9'.bmp\" class=\"c\">", "[9]")
                .replaceAll("<img src=\"../1.bmp\" class=\"c\">", "1")
                .replaceAll("<img src=\"../2.bmp\" class=\"c\">", "2")
                .replaceAll("<img src=\"../3.bmp\" class=\"c\">", "3")
                .replaceAll("<img src=\"../4.bmp\" class=\"c\">", "4")
                .replaceAll("<img src=\"../6.bmp\" class=\"c\">", "6")
                .replaceAll("<img src=\"../7.bmp\" class=\"c\">", "7")
                .replaceAll("<img src=\"../8.bmp\" class=\"c\">", "8")
                .replaceAll("<img src=\"../9.bmp\" class=\"c\">", "9")
                .replaceAll(" <sup>ﾖﾐ</sup>", "")
                .replace("<img src=\"../1'.bmp\">", "[1]")
                .replace("<img src=\"../2'.bmp\">", "[2]")
                .replace("<img src=\"../3'.bmp\">", "[3]")
                .replace("<img src=\"../4'.bmp\">", "[4]")
                .replace("<img src=\"../6'.bmp\">", "[6]")
                .replace("<img src=\"../7'.bmp\">", "[7]")
                .replace("<img src=\"../8'.bmp\">", "[8]")
                .replace("<img src=\"../9'.bmp\">", "[9]")
                .replaceAll("<img src=\"../1.bmp\">", "1")
                .replaceAll("<img src=\"../2.bmp\">", "2")
                .replaceAll("<img src=\"../3.bmp\">", "3")
                .replaceAll("<img src=\"../4.bmp\">", "4")
                .replaceAll("<img src=\"../6.bmp\">", "6")
                .replaceAll("<img src=\"../7.bmp\">", "7")
                .replaceAll("<img src=\"../8.bmp\">", "8")
                .replaceAll("<img src=\"../9.bmp\">", "9")
                .replaceAll(" / -", "</td><td>-")
                .replaceAll(" / \\(", "</td><td>(")
                .replaceAll(" / 0", "</td><td>0")
                .replaceAll(" / 1", "</td><td>1")
                .replaceAll(" / 2", "</td><td>2")
                .replaceAll(" / 3", "</td><td>3")
                .replaceAll(" / 4", "</td><td>4")
                .replaceAll(" / 5", "</td><td>5")
                .replaceAll(" / 6", "</td><td>6")
                .replaceAll(" / 7", "</td><td>7")
                .replaceAll(" / 8", "</td><td>8")
                .replaceAll(" / 9", "</td><td>9")
                .replaceAll("<sup>S</sup>", "s")
                .replaceAll("<sup>Y</sup>", "y")
                .replaceAll("<sup>B</sup>", "b")
                .replaceAll("<sup>G</sup>", "g")
                .replaceAll("<sup>K</sup>", "k")
                .replaceAll("<td align=\"center\">浮</td>", "<td align=\"center\">LNC</td>")
                .replace("<span class=\"command_s bs2 hold\">A+B</span>", "A+B:")
                .replace("<span class=\"command_s bs2 hold\">A+K</span>", "A+K:")
                .replace("<span class=\"command_s bs2 hold\">A+G</span>", "A+G:")
                .replace("<span class=\"command_s bs2 hold\">A+B+K</span>", "A+B+K:")
                .replace("<span class=\"command_s bs2 hold\">A+B+G</span>", "A+B+G:")
                .replace("<span class=\"command_s bs2 hold\">A+K+G</span>", "A+K+G:")
                .replace("<span class=\"command_s bs2 hold\">A+B+K+G</span>", "A+B+K+G:")
                .replace("<span class=\"command_s bs2 hold\">B+K</span>", "B+K:")
                .replace("<span class=\"command_s bs2 hold\">B+G</span>", "B+G:")
                .replace("<span class=\"command_s bs2 hold\">B+K+G</span>", "B+K+G:")
                .replace("<span class=\"command_s bs2 hold\">K+G</span>", "K+G:")
                .replaceAll("<span class=\"command_s bs1 hold\">A</span>", "A:")
                .replaceAll("<span class=\"command_s bs1 hold\">B</span>", "B:")
                .replaceAll("<span class=\"command_s bs1 hold\">K</span>", "K:")
                .replaceAll("<span class=\"command_s bs1 hold\">G</span>", "G:")
                .replaceAll("H H", "H, H")
                .replaceAll("H M", "H, M")
                .replaceAll("H L", "H, L")
                .replaceAll("H Sm", "H, Sm")
                .replaceAll("H Sl", "H, Sl")
                .replaceAll("M H", "M, H")
                .replaceAll("M M", "M, M")
                .replaceAll("M L", "M, L")
                .replaceAll("M Sm", "M, Sm")
                .replaceAll("M Sl", "M, Sl")
                .replaceAll("L H", "L, H")
                .replaceAll("L M", "L, M")
                .replaceAll("L L", "L, L")
                .replaceAll("L Sm", "L, Sm")
                .replaceAll("L Sl", "L, Sl")
                .replaceAll("Sm H", "Sm, H")
                .replaceAll("Sm M", "Sm, M")
                .replaceAll("Sm L", "Sm, L")
                .replaceAll("Sm Sm", "Sm, Sm")
                .replaceAll("Sm Sl", "Sm, Sl")
                .replaceAll("Sl H", "Sl, H")
                .replaceAll("Sl M", "Sl, M")
                .replaceAll("Sl L", "Sl, L")
                .replaceAll("Sl Sm", "Sl, Sm")
                .replaceAll("Sl Sl", "Sl, Sl")
                .replaceAll("! H", "!, H")
                .replaceAll("! M", "!, M")
                .replaceAll("! L", "!, L")
                .replaceAll("! Sm", "!, Sm")
                .replaceAll("! Sl", "!, Sl")
                .replaceAll("AAAAAAA", "A.A.A.A.A.A.A")
                .replaceAll("AAAAAA", "A.A.A.A.A.A")
                .replaceAll("AAAAA", "A.A.A.A.A")
                .replaceAll("AAAA", "A.A.A.A")
                .replaceAll("AAA", "A.A.A")
                .replaceAll("AA", "A.A")
                .replaceAll("AB", "A.B")
                .replaceAll("AK", "A.K")
                .replaceAll("AG", "A.G")
                .replaceAll("BA", "B.A")
                .replaceAll("BBBBBBB", "B.B.B.B.B.B.B")
                .replaceAll("BBBBBB", "B.B.B.B.B.B")
                .replaceAll("BBBBB", "B.B.B.B.B")
                .replaceAll("BBBB", "B.B.B.B")
                .replaceAll("BBB", "B.B.B")
                .replaceAll("BB", "B.B")
                .replaceAll("BK", "B.K")
                .replaceAll("BG", "B.G")
                .replaceAll("KA", "K.A")
                .replaceAll("KB", "K.B")
                .replaceAll("KKKKKKK", "K.K.K.K.K.K.K")
                .replaceAll("KKKKKK", "K.K.K.K.K.K")
                .replaceAll("KKKKK", "K.K.K.K.K")
                .replaceAll("KKKK", "K.K.K.K")
                .replaceAll("KKK", "K.K.K")
                .replaceAll("KK", "K.K")
                .replaceAll("KG", "K.G")
                .replaceAll("GA", "G.A")
                .replaceAll("GB", "G.B")
                .replaceAll("GK", "G.K")
                .replaceAll("GG", "G.G")
                .replaceAll("A\\[", "A.\\[")
                .replaceAll("B\\[", "B.\\[")
                .replaceAll("K\\[", "K.\\[")
                .replaceAll("G\\[", "G.\\[")
                .replaceAll("A1", "A.1")
                .replaceAll("A2", "A.2")
                .replaceAll("A3", "A.3")
                .replaceAll("A4", "A.4")
                .replaceAll("A5", "A.5")
                .replaceAll("A6", "A.6")
                .replaceAll("A7", "A.7")
                .replaceAll("A8", "A.8")
                .replaceAll("A9", "A.9")
                .replaceAll("B1", "B.1")
                .replaceAll("B2", "B.2")
                .replaceAll("B3", "B.3")
                .replaceAll("B4", "B.4")
                .replaceAll("B5", "B.5")
                .replaceAll("B6", "B.6")
                .replaceAll("B7", "B.7")
                .replaceAll("B8", "B.8")
                .replaceAll("B9", "B.9")
                .replaceAll("K1", "K.1")
                .replaceAll("K2", "K.2")
                .replaceAll("K3", "K.3")
                .replaceAll("K4", "K.4")
                .replaceAll("K5", "K.5")
                .replaceAll("K6", "K.6")
                .replaceAll("K7", "K.7")
                .replaceAll("K8", "K.8")
                .replaceAll("K9", "K.9")
                .replaceAll("G1", "G.1")
                .replaceAll("G2", "G.2")
                .replaceAll("G3", "G.3")
                .replaceAll("G4", "G.4")
                .replaceAll("G5", "G.5")
                .replaceAll("G6", "G.6")
                .replaceAll("G7", "G.7")
                .replaceAll("G8", "G.8")
                .replaceAll("G9", "G.9")
                .replaceAll("\\]A", "\\].A")
                .replaceAll("\\]B", "\\].B")
                .replaceAll("\\]K", "\\].K")
                .replaceAll("\\]G", "\\].G")
                .replaceAll("\\]\\[", "\\].\\[")
                .replaceAll("\\]1", "\\].1")
                .replaceAll("\\]2", "\\].2")
                .replaceAll("\\]3", "\\].3")
                .replaceAll("\\]4", "\\].4")
                .replaceAll("\\]5", "\\].5")
                .replaceAll("\\]6", "\\].6")
                .replaceAll("\\]7", "\\].7")
                .replaceAll("\\]8", "\\].8")
                .replaceAll("\\]9", "\\].9")
                .replaceAll("&plusmn;0", "0")
                .replaceAll("SC中", "SC ")
                .replaceAll("背向けA", "BT A")
                .replaceAll("背向けB", "BT B")
                .replaceAll("背向けK", "BT K")
                .replaceAll("背向けG", "BT G")
                .replaceAll("背向け\\[", "BT [")
                .replaceAll("背向け1", "BT 1")
                .replaceAll("背向け2", "BT 2")
                .replaceAll("背向け3", "BT 3")
                .replaceAll("背向け4", "BT 4")
                .replaceAll("背向け5", "BT 5")
                .replaceAll("背向け6", "BT 6")
                .replaceAll("背向け7", "BT 7")
                .replaceAll("背向け8", "BT 8")
                .replaceAll("背向け9", "BT 9")
                .replaceAll("しゃがみA", "FC A")
                .replaceAll("しゃがみB", "FC B")
                .replaceAll("しゃがみK", "FC K")
                .replaceAll("しゃがみG", "FC G")
                .replaceAll("しゃがみ\\[", "FC [")
                .replaceAll("しゃがみ1", "FC 1")
                .replaceAll("しゃがみ2", "FC 2")
                .replaceAll("しゃがみ3", "FC 3")
                .replaceAll("しゃがみ4", "FC 4")
                .replaceAll("しゃがみ5", "FC 5")
                .replaceAll("しゃがみ6", "FC 6")
                .replaceAll("しゃがみ7", "FC 7")
                .replaceAll("しゃがみ8", "FC 8")
                .replaceAll("しゃがみ9", "FC 9")
                .replaceAll("立ち途中A", "WR A")
                .replaceAll("立ち途中B", "WR B")
                .replaceAll("立ち途中K", "WR K")
                .replaceAll("立ち途中G", "WR G")
                .replaceAll("立ち途中\\[", "WR [")
                .replaceAll("立ち途中1", "WR 1")
                .replaceAll("立ち途中2", "WR 2")
                .replaceAll("立ち途中3", "WR 3")
                .replaceAll("立ち途中4", "WR 4")
                .replaceAll("立ち途中5", "WR 5")
                .replaceAll("立ち途中6", "WR 6")
                .replaceAll("立ち途中7", "WR 7")
                .replaceAll("立ち途中8", "WR 8")
                .replaceAll("立ち途中9", "WR 9")
                .replaceAll("ジャンプ中A", "JUMP A")
                .replaceAll("ジャンプ中B", "JUMP B")
                .replaceAll("ジャンプ中K", "JUMP K")
                .replaceAll("ジャンプ中G", "JUMP G")
                .replaceAll("ジャンプ中\\[", "JUMP [")
                .replaceAll("ジャンプ中1", "JUMP 1")
                .replaceAll("ジャンプ中2", "JUMP 2")
                .replaceAll("ジャンプ中3", "JUMP 3")
                .replaceAll("ジャンプ中4", "JUMP 4")
                .replaceAll("ジャンプ中5", "JUMP 5")
                .replaceAll("ジャンプ中6", "JUMP 6")
                .replaceAll("ジャンプ中7", "JUMP 7")
                .replaceAll("ジャンプ中8", "JUMP 8")
                .replaceAll("ジャンプ中9", "JUMP 9")
                .replaceAll("ディレイA", ", Delay A")
                .replaceAll("ディレイB", ", Delay B")
                .replaceAll("ディレイK", ", Delay K")
                .replaceAll("ディレイG", ", Delay G")
                .replaceAll("ディレイ\\[", ", Delay \\[")
                .replaceAll("ディレイ1", ", Delay 1")
                .replaceAll("ディレイ2", ", Delay 2")
                .replaceAll("ディレイ3", ", Delay 3")
                .replaceAll("ディレイ4", ", Delay 4")
                .replaceAll("ディレイ5", ", Delay 5")
                .replaceAll("ディレイ6", ", Delay 6")
                .replaceAll("ディレイ7", ", Delay 7")
                .replaceAll("ディレイ8", ", Delay 8")
                .replaceAll("ディレイ9", ", Delay 9")
                .replaceAll("ジャスト", "(Just)")
                .replaceAll("打投", "Throw")
                .replaceAll("ガード硬直", "Block: ")
                .replace("<span class=\"sp ml\">技L</span>", " KNB: ")
                .replace("弱", "Weak")
                .replace("<span class=\"sp js\">JS</span>", ", JS: ")
                .replace("<span class=\"sp gc\">GC</span>", ", GC: ")
                .replace("<span class=\"sp cs\">CS</span>", ", CS: ")
                .replace("ディレイ幅", ", Delayable ")
                .replace("ヒット確認猶予", "Hit confirm delayable ")
                .replaceAll("位置入れ替え", " Switch sides ")
                .replaceAll("ガード時、", "On block,")
                .replaceAll("ヒット時、", "On hit,")
                .replaceAll("1段目から連続ヒット", ", NC")
                .replaceAll("<span style=\"color:dimgray;\">", "")
                .replaceAll("%</span>", "%,")
                .replaceAll("1 or 2段目カウンターから連続ヒット", ", First two hits NCC, Second two hits NCC")
                .replaceAll("2 or 3段目カウンターから連続ヒット", ", Second two hits NCC, Third two hits NCC")
                .replaceAll("1 or 3段目カウンターから連続ヒット", ", First two hits NCC, Third two hits NCC")
                .replaceAll("1段目カウンターから連続ヒット", ", First two hits NCC")
                .replaceAll("2段目カウンターから連続ヒット", ", Second two hits NCC")
                .replaceAll("3段目カウンターから連続ヒット", ", Third two hits NCC")
                .replaceAll("1 or 2段目から連続ヒット", ", First two hits NC, Second two hits NC")
                .replaceAll("2 or 3段目から連続ヒット", ", Second two hits NC, Third two hits NC")
                .replaceAll("1 or 3段目から連続ヒット", ", First two hits NC, Third two hits NC")
                .replaceAll("1段目から連続ヒット", ", First two hits NC")
                .replaceAll("2段目から連続ヒット", ", Second two hits NC")
                .replaceAll("3段目から連続ヒット", ", Third two hits NC")
                .replaceAll("カウンターで強制しゃがみ", "")
                .replaceAll("ダメージ()", ", Damage ()")
                .replaceAll("リングアウト可能", "RO possible")
                .replaceAll("しゃがみ帰着", "Ends FC")
                .replaceAll("発生", "i")
                .replaceAll("から空中判定", " TJ")
                .replaceAll("<input type=\"checkbox\" id=\"label[0-9]\" class=\"cssuacc\" /><label for=\"labe[0-9]\">旧アップデート</label>", "")
                .replaceAll("<input type=\"checkbox\" id=\"label[0-9][0-9]\" class=\"cssuacc\" /><label for=\"labe[0-9][0-9]\">旧アップデート</label>", "")
                .replaceAll("<input type=\"checkbox\" id=\"label[0-9][0-9][0-9]\" class=\"cssuacc\" /><label for=\"labe[0-9][0-9][0-9]\">旧アップデート</label>", "")
                .replace("<td>, ", "<td>")
                .replace("<br /> ,", ", ")
                .replaceAll("RE中", "RE ")
                .replace(">B.A<", ">BA<");
        
        try (PrintWriter out = new PrintWriter(Files.newBufferedWriter(Paths.get(str), StandardCharsets.UTF_8))) {
            out.println(output);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        ArrayList<Attack_command> attack_list = new ArrayList<>();
        
        Document fr = Jsoup.parse(output);
        Elements tables = fr.select("table[class=fr]");
        for(int iter=0; iter < tables.size(); iter++) {
            Element table = tables.get(iter);
            Elements rows = table.select("tr");
            for (int i = 2; i < rows.size(); i++) { //first row is the col names so skip it.
                Attack_command attack_command = new Attack_command();

                Element row = rows.get(i);
                Elements cols = row.select("td");

                Elements tooltip = cols.get(0).select("span[class=tooltip]");
                if(tooltip.size() == 0) {
                    Elements sps = cols.get(0).select("span");
                    if(!sps.isEmpty()) {
                        for(int i3=0; i3 < sps.size(); i3++) {
                            if("".equals(attack_command.getNotes())) {
                                attack_command.setNotes(sps.get(i3).text());
                            } else {
                                attack_command.setNotes(attack_command.getNotes() + ", " + sps.get(i3).text());
                            }
                        }

                        String[] name = cols.get(0).after("span").html().split("<br> ");
                        attack_command.setJp_name(name[1].replace("i", "発生"));
                        System.out.println("KILLME::::" +name[1]);
                        System.out.println("KILLME::::" +name[2]);
                        attack_command.setAttack_command(name[2]);
                        attack_command.setProperty(name[3]);

                    } else {
                        String[] name = cols.get(0).html().split("<br> ");
                        System.out.println("KILLL ME: " + name[0]);
                        attack_command.setJp_name(name[0].replace("i", "発生"));
                        attack_command.setAttack_command(name[1]);
                        attack_command.setProperty(name[2]);
                    }
                } else {
                    attack_command.setJp_name(tooltip.get(0).text().replace("i", "発生"));
                    Elements sps = cols.get(0).select("span");
                    if(sps.size() > 1) {
                        for(int i3=0; i3 < sps.size()-1; i3++) {
                            if("".equals(attack_command.getNotes())) {
                                attack_command.setNotes(sps.get(i3).text());
                            } else {
                                attack_command.setNotes(attack_command.getNotes() + ", " + sps.get(i3).text());
                            }
                        }

                        String[] name = cols.get(0).after("span").html().split("<br> ");
                        attack_command.setAttack_command(name[2]);
                        attack_command.setProperty(name[3]);

                    } else {
                        String[] name = cols.get(0).html().split("<br> ");
                       
                        attack_command.setAttack_command(name[1]);
                        attack_command.setProperty(name[2]);
                    }
                }
                attack_command.setDamage(cols.get(1).text());
                attack_command.setStartup(cols.get(2).text());
                attack_command.setBlock(cols.get(3).text());
                attack_command.setHit(cols.get(4).text());
                attack_command.setCounter_hit(cols.get(5).text());
                if(cols.get(6).hasText()) {
                    if("".equals(attack_command.getNotes())) {
                            attack_command.setNotes("Ends i" + cols.get(6).text());
                        } else {
                            attack_command.setNotes(attack_command.getNotes() + ", Ends i" + cols.get(6).text());
                        }
                }
                if(cols.get(7).hasText()) {
                    if("".equals(attack_command.getNotes())) {
                            attack_command.setNotes(cols.get(7).text());
                        } else {
                            attack_command.setNotes(attack_command.getNotes() + ", " + cols.get(7).text());
                        }
                }
                attack_list.add(attack_command);
                System.out.println(attack_command.toString());


            }
        }
        int last = attack_list.size()-1;
        
        String json = "{\"name\": \"\",\"full_name\": \"\",\"jp_name\": \"\",\"version\": \"\",\"attack_list\": [";
        for(int iter=0; iter<last; iter++) {
            json = json
                + "{\"attack_command\": \"" + attack_list.get(iter).getAttack_command() + "\","
                + "\"attack_name\": \"" + attack_list.get(iter).getAttack_name() + "\","
                + "\"jp_name\": \"" + attack_list.get(iter).getJp_name() + "\","
                + "\"property\": \"" + attack_list.get(iter).getProperty() + "\","
                + "\"damage\": \"" + attack_list.get(iter).getDamage() + "\","
                + "\"startup\": \"" + attack_list.get(iter).getStartup() + "\","
                + "\"block\": \"" + attack_list.get(iter).getBlock() + "\","
                + "\"hit\": \"" + attack_list.get(iter).getHit() + "\","
                + "\"counter_hit\": \"" + attack_list.get(iter).getCounter_hit() + "\","
                + "\"chip_damage\": \"" + attack_list.get(iter).getChip_damage() + "\","
                + "\"guard_break\": \"" + attack_list.get(iter).getGuard_break() + "\","
                + "\"notes\": \"" + attack_list.get(iter).getNotes() + "\","
                + "\"index\": \"" + attack_list.get(iter).getIndex() + "\"},";
        }
        json = json
                + "{\"attack_command\": \"" + attack_list.get(last).getAttack_command() + "\","
                + "\"attack_name\": \"" + attack_list.get(last).getAttack_name() + "\","
                + "\"jp_name\": \"" + attack_list.get(last).getJp_name() + "\","
                + "\"property\": \"" + attack_list.get(last).getProperty() + "\","
                + "\"damage\": \"" + attack_list.get(last).getDamage() + "\","
                + "\"startup\": \"" + attack_list.get(last).getStartup() + "\","
                + "\"block\": \"" + attack_list.get(last).getBlock() + "\","
                + "\"hit\": \"" + attack_list.get(last).getHit() + "\","
                + "\"counter_hit\": \"" + attack_list.get(last).getCounter_hit() + "\","
                + "\"chip_damage\": \"" + attack_list.get(last).getChip_damage() + "\","
                + "\"guard_break\": \"" + attack_list.get(last).getGuard_break() + "\","
                + "\"notes\": \"" + attack_list.get(last).getNotes() + "\","
                + "\"index\": \"" + attack_list.get(last).getIndex() + "\"}]}";
        
        try (PrintWriter out = new PrintWriter(Files.newBufferedWriter(Paths.get(str2), StandardCharsets.UTF_8))) {
            out.println(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //*/
        /*
        
        Element tableA = fr.select("table").first();
        //System.out.println("tableA: " + tableA.size());
        Elements rows = tableA.select("tr");
        for (int i = 2; i < rows.size(); i++) { //first row is the col names so skip it.
            Attack_command attack_command = new Attack_command();
            
            Element row = rows.get(i);
            Elements cols = row.select("td");
            
            Elements sps = cols.get(0).select("span");
            if(!sps.isEmpty()) {
                for(int i3=0; i3 < sps.size(); i3++) {
                    if("".equals(attack_command.getNotes())) {
                        attack_command.setNotes(sps.get(i3).text());
                    } else {
                        attack_command.setNotes(attack_command.getNotes() + ", " + sps.get(i3).text());
                    }
                }
                
                String[] name = cols.get(0).after("span").html().split("<br> ");
                attack_command.setJp_name(name[1]);
                attack_command.setAttack_command(name[2]);
                attack_command.setProperty(name[3]);
                
            } else {
                String[] name = cols.get(0).html().split("<br> ");
                attack_command.setJp_name(name[0]);
                attack_command.setAttack_command(name[1]);
                attack_command.setProperty(name[2]);
            }
            attack_command.setDamage(cols.get(1).text());
            attack_command.setStartup(cols.get(2).text());
            attack_command.setBlock(cols.get(3).text());
            attack_command.setHit(cols.get(4).text());
            attack_command.setCounter_hit(cols.get(5).text());
            if(cols.get(6).hasText()) {
                if("".equals(attack_command.getNotes())) {
                        attack_command.setNotes("Ends i" + cols.get(6).text());
                    } else {
                        attack_command.setNotes(attack_command.getNotes() + ", Ends i" + cols.get(6).text());
                    }
            }
            if(cols.get(7).hasText()) {
                if("".equals(attack_command.getNotes())) {
                        attack_command.setNotes(cols.get(7).text());
                    } else {
                        attack_command.setNotes(attack_command.getNotes() + ", " + cols.get(7).text());
                    }
            }
            System.out.println(attack_command.toString());
            
            
        }
        //PrintStream out = new PrintStream(System.out, true, "UTF-8");
        //System.out.print(new String("リングアウト可能"));
            
        */
        /*
        TABLE ROW..................
        <tr>
            <td class="labeled">
                <span class="sp sc">SC</span>
                <span class="sp ba">B.A</span>
                <br/>
                ボルカ・エル・パグ～AR<br/>
                SC A.A+B.[B]<br/>H, H, M, M(Special)
            </td>
            <td>8,11,15,33</td>
            <td align="center"></td>
            <td align="center">+10</td>
            <td align="center">LNC</td>
            <td align="center">LNC</td>
            <td align="center"></td>
            <td>Third two hits NC</td>
        </tr>
        OUTPUT...................
        ___________________________
        Attack Name: 
        Attack Command: SC A.A+B.[B]
        JP Name: ボルカ・エル・パグ～AR
        Property: H, H, M, M(Special)
        Damage: 8,11,15,33
        Start-Up: 
        Block: +10
        NH: LNC
        CH: LNC
        Chip: 
        GB: 
        Notes: SC, B.A, Third two hits NC
        Index: 
        */
        
    }

}

































































































